﻿using System.Collections.Generic;
using uk.plingo.shift.core.entities;
using uk.plingo.shift.core.interfaces.entities;

namespace uk.plingo.shift.data.test.helpers
{
    public static class CalendarDatabaseHelper
    {
        public static List<Calendar> DefaultItems()
        {
            List<Calendar> calendars = new List<Calendar>();
            calendars.Add(CreateCalendarItem(1, Entity.EntityState.Active));
            calendars.Add(CreateCalendarItem(2, Entity.EntityState.Deleted));
            return calendars;
        }

        public static Calendar DefautCalendarItem()
        {
            return CreateCalendarItem(1, Entity.EntityState.Active);
        }

        public static Calendar CreateCalendarItem(int itemId, Entity.EntityState state)
        {
            return new Calendar()
            {
                id = itemId,
                Status = state
            };
        }
    }
}