﻿using System.Collections.Generic;
using uk.plingo.shift.core.entities;
using uk.plingo.shift.core.interfaces.entities;

namespace uk.plingo.shift.data.test.helpers
{
    public static class EventTypeDatabaseHelper
    {
        public static List<EventType> DefaultItems()
        {
            List<EventType> eventTypes = new List<EventType>();
            eventTypes.Add(CreateEventTypeItem(1, Entity.EntityState.Active));
            eventTypes.Add(CreateEventTypeItem(2, Entity.EntityState.Deleted));
            return eventTypes;
        }

        public static EventType DefautEventTypeItem()
        {
            return CreateEventTypeItem(1, Entity.EntityState.Active);
        }

        public static EventType CreateEventTypeItem(int itemId, Entity.EntityState state)
        {
            return new EventType()
            {
                id = itemId,
                Status = state
            };
        }
    }
}