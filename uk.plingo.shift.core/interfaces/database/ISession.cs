﻿using System.Collections.Generic;
using uk.plingo.shift.core.interfaces.entities;

namespace uk.plingo.shift.core.interfaces.database
{
    public interface ISession
    {
        public T AddOrUpdate<T>(T item) where T : Entity;

        public IEnumerable<T> CreateQuery<T>() where T : Entity;
    }
}