﻿using System;

namespace uk.plingo.shift.core.interfaces.entities
{
    public abstract class Entity
    {
        public enum EntityState : byte
        {
            Deleted = 0,
            Active = 1,
        }

        public int id { get; set; }
        public DateTime DateCreated { get; set; }
        public int UserCreated { get; set; }
        public EntityState Status { get; set; }
    }
}