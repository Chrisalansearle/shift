﻿using uk.plingo.shift.core.entities;

namespace uk.plingo.shift.core.interfaces.data
{
    public interface IEventTypeDao : IBaseDao<EventType>
    {
    }
}