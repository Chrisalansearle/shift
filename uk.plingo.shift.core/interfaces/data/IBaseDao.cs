﻿using System.Collections.Generic;
using uk.plingo.shift.core.interfaces.entities;

namespace uk.plingo.shift.core.interfaces.data
{
    public interface IBaseDao<T> where T : Entity
    {
        public T Add(T item);

        public T GetById(int id);

        public IEnumerable<T> GetByStatus(Entity.EntityState state);

        public IEnumerable<T> GetAll();
    }
}