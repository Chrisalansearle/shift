﻿using System;
using uk.plingo.shift.core.interfaces.entities;

namespace uk.plingo.shift.core.entities
{
    public class Calendar : Entity
    {
        public string EventName { get; set; }
        public string EventNotes { get; set; }
        public DateTime EventDateTime { get; set; }
        public int? EventDuration { get; set; }
    }
}