﻿using System;
using System.Collections.Generic;
using System.Linq;
using uk.plingo.shift.core.interfaces.database;
using uk.plingo.shift.core.interfaces.entities;

namespace uk.plingo.shift.data.entities
{
    public abstract class BaseDao<T> where T : Entity
    {
        public ISession Session { get; private set; }

        protected BaseDao(ISession session)
        {
            if (session == null)
                throw new ArgumentNullException(nameof(session));

            Session = session;
        }

        public T GetById(int id)
        {
            return GetAll()
                .FirstOrDefault(x => x.id == id);
        }

        public IEnumerable<T> GetByStatus(Entity.EntityState state)
        {
            return GetAll()
                .Where(x => x.Status == state);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Session.CreateQuery<T>();
        }

        public T Add(T item)
        {
            return Session.AddOrUpdate(item);
        }
    }
}