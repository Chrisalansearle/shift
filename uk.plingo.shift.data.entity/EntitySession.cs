﻿using System.Collections.Generic;
using System.Data.Entity;
using uk.plingo.shift.core.entities;
using uk.plingo.shift.core.interfaces;
using uk.plingo.shift.core.interfaces.database;
using uk.plingo.shift.core.interfaces.entities;
using uk.plingo.shift.data.entity.mapping;

namespace uk.plingo.shift.data.entity
{
    public class EntitySession : DbContext, ISession
    {
        private IDatabase _dao;

        public enum SessionState
        {
            SaveCanges,
            Readonly
        }

        private readonly SessionState _sessionState;

        public EntitySession(IDatabase daoFactory, SessionState sessionState) : base(nameof(EntitySession))
        {
            _dao = daoFactory;
            _sessionState = sessionState;

            _dao.SetSession(this);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            CalendarMapping.Map(modelBuilder.Entity<Calendar>());
            EventTypeMapping.Map(modelBuilder.Entity<EventType>());
        }

        protected override void Dispose(bool disposing)
        {
            // if we are monitoring changes, then this will
            // allow us to automatically save the changes... as why wouldn't we!
            if (_sessionState == SessionState.SaveCanges)
            {
                SaveChanges();
            }

            _dao.ResetSession();
            _dao = null;

            base.Dispose(disposing);
        }

        public virtual IEnumerable<T> CreateQuery<T>() where T : Entity
        {
            if (_sessionState == SessionState.Readonly)
                return this.Set<T>().AsNoTracking();
            else
                return this.Set<T>();
        }

        public T AddOrUpdate<T>(T item) where T : Entity
        {
            if (_sessionState == SessionState.SaveCanges)
            {
                T foundItem = Set<T>().Find(item.id);
                if (foundItem == null)
                {
                    //INSERT
                    foundItem = Set<T>().Add(item);
                }
                else
                {
                    //UPDATE
                    foundItem = item;
                }

                return foundItem;
            }
            else
            {
                return item;
            }
        }
    }
}