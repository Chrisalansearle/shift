﻿using System.Data.Entity.ModelConfiguration;
using uk.plingo.shift.core.entities;
using uk.plingo.shift.data.entity.helper;

namespace uk.plingo.shift.data.entity.mapping
{
    public static class CalendarMapping
    {
        public static void Map(EntityTypeConfiguration<Calendar> builder)
        {
            builder.Property(c => c.EventName)
                .IsUnicode(false);

            builder.Property(c => c.EventNotes)
                .IsUnicode(false);

            builder.Property(c => c.Status)
                .HasColumnType(DatabaseTypes.TinyInt);
        }
    }
}