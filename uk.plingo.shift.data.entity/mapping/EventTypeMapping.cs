﻿using System.Data.Entity.ModelConfiguration;
using uk.plingo.shift.core.entities;
using uk.plingo.shift.data.entity.helper;

namespace uk.plingo.shift.data.entity.mapping
{
    public static class EventTypeMapping
    {
        public static void Map(EntityTypeConfiguration<EventType> builder)
        {
            builder.HasKey(c => c.id);

            builder.Property(c => c.EventTypeName)
                .IsUnicode(false);

            builder.Property(c => c.StartTime).IsRequired();

            builder.Property(c => c.EventNotes)
                .IsUnicode(false);

            builder.Property(c => c.EventDuration);

            builder.Property(c => c.DateCreated).IsRequired();

            builder.Property(c => c.UserCreated).IsRequired();

            builder.Property(c => c.Status)
                .HasColumnType(DatabaseTypes.TinyInt)
                .IsRequired();
        }
    }
}