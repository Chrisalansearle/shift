using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using uk.plingo.shift.data.test;
using uk.plingo.shift.logic;

namespace uk.plingo.shift.test.logic
{
    [TestClass]
    public class CalendarLogicTests
    {
        [TestMethod]
        public void CreateMethod()
        {
            MockDatabase mockDatabase = new MockDatabase();

            CalendarLogic calendarLogic = new CalendarLogic(mockDatabase);
            calendarLogic.CreateCalendarEvent("test event", "some notes", DateTime.Now, 10, 1);

            var total = mockDatabase.CalendarDao.GetAll().Count();
            Assert.AreEqual(3, total);

            var thirdItem = mockDatabase.CalendarDao.GetById(3);
            Assert.IsNotNull(thirdItem);
            Assert.AreEqual(3, thirdItem.id);
        }
    }
}