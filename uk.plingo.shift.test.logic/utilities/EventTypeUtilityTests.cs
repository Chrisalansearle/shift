using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using uk.plingo.shift.logic.utilities;

namespace uk.plingo.shift.test.logic.utiities
{
    [TestClass]
    public class EventTypeUtilityTests
    {
        [TestMethod]
        public void CreateMethod()
        {
            var eventType = EventTypeUtility.CreateEventType("test event", "some notes", new TimeSpan(1, 0, 0), 60, 1);

            Assert.IsNotNull(eventType);
            Assert.AreEqual(0, eventType.id);
            Assert.AreEqual("test event", eventType.EventTypeName);
            Assert.AreEqual("some notes", eventType.EventNotes);
            Assert.AreEqual(60, eventType.EventDuration);
            Assert.AreEqual(core.interfaces.entities.Entity.EntityState.Active, eventType.Status);
        }
    }
}