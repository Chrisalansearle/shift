using Microsoft.VisualStudio.TestTools.UnitTesting;
using uk.plingo.shift.core.entities;
using uk.plingo.shift.core.interfaces.entities;
using uk.plingo.shift.data.test.helpers;
using uk.plingo.shift.logic.utilities;

namespace uk.plingo.shift.test.logic.utiities
{
    [TestClass]
    public class BaseUtilityTests
    {
        [TestMethod]
        public void CreateMethod()
        {
            Calendar calendar = CalendarDatabaseHelper.DefautCalendarItem();

            BaseUtility<Calendar>.Active(calendar);
            Assert.AreEqual(Entity.EntityState.Active, calendar.Status);

            BaseUtility<Calendar>.Delete(calendar);
            Assert.AreEqual(Entity.EntityState.Deleted, calendar.Status);

            BaseUtility<Calendar>.Active(calendar);
            Assert.AreEqual(Entity.EntityState.Active, calendar.Status);

            BaseUtility<Calendar>.UpdateStatus(calendar, Entity.EntityState.Deleted);
            Assert.AreEqual(Entity.EntityState.Deleted, calendar.Status);
        }
    }
}