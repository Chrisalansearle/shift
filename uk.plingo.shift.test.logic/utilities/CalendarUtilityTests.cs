using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using uk.plingo.shift.logic.utilities;

namespace uk.plingo.shift.test.logic.utiities
{
    [TestClass]
    public class CalendarUtilityTests
    {
        [TestMethod]
        public void CreateMethod()
        {
            var calendar = CalendarUtility.CreateCalendar("test event", "some notes", DateTime.Now, 10, 1);

            Assert.IsNotNull(calendar);
            Assert.AreEqual(0, calendar.id);
            Assert.AreEqual("test event", calendar.EventName);
            Assert.AreEqual("some notes", calendar.EventNotes);
            Assert.AreEqual(10, calendar.EventDuration);
            Assert.AreEqual(core.interfaces.entities.Entity.EntityState.Active, calendar.Status);
        }
    }
}