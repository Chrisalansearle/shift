﻿using System;
using uk.plingo.shift.core.interfaces;
using uk.plingo.shift.logic.utilities;

namespace uk.plingo.shift.logic
{
    public class CalendarLogic : BaseLogic
    {
        public CalendarLogic(IDatabase database) : base(database)
        {
        }

        public void CreateCalendarEvent(string eventName, string eventNotes, DateTime eventDateTime, int eventDuration, int userCreatedId)
        {
            var calendar = CalendarUtility.CreateCalendar(eventName, eventNotes, eventDateTime, eventDuration, userCreatedId);
            Database.CalendarDao.Add(calendar);
        }
    }
}