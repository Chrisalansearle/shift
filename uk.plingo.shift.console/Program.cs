﻿using System;
using System.Linq;
using uk.plingo.shift.core.entities;
using uk.plingo.shift.core.interfaces;
using uk.plingo.shift.data.entity;
using uk.plingo.shift.entity.data;

namespace uk.plingo.shift.console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            EntityDatabase database = new EntityDatabase();

            Console.WriteLine("Hello World!");
            Count(database);
            CreateNew(database, EntitySession.SessionState.Readonly);
            CreateNew(database, EntitySession.SessionState.SaveCanges);
            Console.ReadLine();
        }

        private static void CreateNew(IDatabase database, EntitySession.SessionState session)
        {
            Console.WriteLine($"Detect Changes : {session}");

            using (var db = new EntitySession(database, session))
            {
                var calendar = new Calendar()
                {
                    DateCreated = DateTime.Now,
                    EventDateTime = DateTime.Now,
                    EventDuration = 10,
                    EventName = "Hello World",
                    EventNotes = "Hello hello",
                    Status = core.interfaces.entities.Entity.EntityState.Active,
                    UserCreated = 1
                };

                database.CalendarDao.Add(calendar);
            }

            Count(database);
        }

        private static void Count(IDatabase database)
        {
            using (var db = new EntitySession(database, EntitySession.SessionState.Readonly))
            {
                var total = database.CalendarDao.GetAll();
                Console.WriteLine("Total {0} :-)", total.Count());
            }
        }
    }
}